from django.db import models
from django.utils import timezone
import datetime
import string
import random
import hashlib

def rand_submitter_gen():
    r = ''.join(random.choice(string.lowercase[:6] + string.digits) for i in range(40));
    return r;
    
def comment_id_txt():
    m = hashlib.new('sha1');
    m.update(rand_submitter_gen());
    return m.hexdigest()[:6];

# Create your models here.
class Question(models.Model):
    question_text = models.CharField(max_length=200);
    pub_date = models.DateTimeField('date published', default=timezone.now);
    tagline = models.CharField(max_length=140, default="", blank=True);
    submitter = models.CharField(max_length=40, default=rand_submitter_gen);
    submitter_nick = models.CharField(max_length=20, default="", blank=True);
    description = models.CharField(max_length=10000, blank=True);
    last_modified = models.DateTimeField(auto_now=True, auto_now_add=True);
    sticky = models.BooleanField(default=False);
    
    def was_published_recently(self):
        return self.pub_date >= timezone.now() - datetime.timedelta(days=1);
        
    was_published_recently.admin_order_field = 'pub_date';
    was_published_recently.boolean = True;
    was_published_recently.short_description = 'Published recently?';
    
    def __unicode__(self):
        return self.question_text;
    
class Choice(models.Model):
    question = models.ForeignKey(Question);
    choice_text = models.CharField(max_length=200);
    votes = models.IntegerField(default=0);
    
    def __unicode__(self):
        return self.choice_text;
        
class Comment(models.Model):
    question = models.ForeignKey(Question, related_name="comments");
    submitter = models.CharField(max_length=40);
    submitter_nick = models.CharField(max_length=20, default="", blank=True);
    text = models.CharField(max_length=10000);
    id_txt = models.CharField(max_length=6, default=comment_id_txt);
    
    def __unicode__(self):
        return self.text;
        
class Nick(models.Model):
    name = models.CharField(max_length=20);
    password_hash = models.CharField(max_length=40);
    admin = models.BooleanField(default=False);
    
    def verify_password(self, password):
        if(hashlib.sha1(password).hexdigest() == self.password_hash):
            return True;
        return False;
    
    '''
    Create a nick object with a password instead of a password hash
    '''
    @classmethod
    def create_password(cls, nick, password):
        return cls(name = nick, password_hash = hashlib.sha1(password).hexdigest());
    
    def __unicode__(self):
        return self.name;