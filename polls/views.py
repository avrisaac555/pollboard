from django.shortcuts import render, get_object_or_404
from utils.views.shortcuts import base_context_builder, check_admin
from django.http import HttpResponseRedirect, HttpResponseServerError, HttpResponseBadRequest, HttpResponseForbidden, HttpResponseNotFound
from django.template import RequestContext
from django.contrib import messages
from django.views.generic import View
from polls.models import Question, Choice, Comment, Nick
from django.core.urlresolvers import reverse
from django.core.exceptions import ValidationError
import polls.utils.sanitizer as sanitizer
import hashlib
import random
import datetime

def index(request):
    sticky_question_list = Question.objects.order_by('-id').filter(sticky=True);
    question_list = Question.objects.order_by('-last_modified').exclude(sticky=True)[:20];
    
    return render(request, 'polls/index.html', base_context_builder(request, {'latest_question_list':question_list, 'sticky_question_list':sticky_question_list}));
        
def detail(request, question_id):
    uid = request.session.get('uid', 0);
    q = get_object_or_404(Question, pk=question_id);
    is_author = False;
    
    if(q.submitter == uid):
        is_author = True
    
    return render(request, 'polls/detail.html', base_context_builder(request,{'question': q, 'author': is_author}));

def results(request, question_id):
    q = get_object_or_404(Question, pk=question_id);
    
    return render(request, 'polls/results.html', base_context_builder(request, {'question': q,}));

def login(request):
    h = hashlib.sha1();
    h.update(random.random().__str__());
    request.session['uid'] = h.hexdigest();
    
    return HttpResponseRedirect(request.GET.get("next", reverse('polls:index')));

def vote(request, question_id):
    #Try and get the question from the id, based on parameter passed from URL
    p = get_object_or_404(Question, pk=question_id);
    
    try:
        selected_choice = p.choice_set.get(pk=request.POST['choice']);
    except (KeyError, Choice.DoesNotExist):
        return render(request, 'polls/detail.html', {'question': p});
    else:
        selected_choice.votes += 1;
        selected_choice.save();
        return HttpResponseRedirect(reverse('polls:results', args=(p.id,)));
        
def post_poll(request):
    if (request.POST.get('title', 0) != 0):
        try:
            question = Question(
                question_text=request.POST['title'],
                tagline=sanitizer.clean_all(request.POST['tagline']),
                submitter=request.session['uid'],
                submitter_nick=request.session.get('nick', ""),
                description=sanitizer.clean(request.POST['description'])
            );
            #SQLite does not enforce max_characters, so we need to validate here
            question.full_clean();
            question.save();
        
            for choice_post in request.POST.getlist('choice'):
                if(choice_post != ""):
                    c = Choice(question=question, choice_text=choice_post);
                    c.full_clean();
                    c.save();
        except KeyError:
            return HttpResponseBadRequest();
        except ValidationError, e:
            #Error validating, so we return a prettyfied error message :)
            name = None;
            for key in e:
                if (key[0] == 'question_text'):
                    name = "Title";
                elif(key[0] == 'Choice_Text'):
                    name = "Choice";
                else:
                    name = key[0].title();
                messages.error(request, name + " - " + key[1][0]);
                
            return HttpResponseRedirect(reverse('polls:post'));
        else:
            return HttpResponseRedirect(reverse('polls:index'));
    else:
        return render(request, 'polls/post.html', base_context_builder(request));
        
def question_delete(request, question_id):
    q = get_object_or_404(Question, id=question_id);
    
    #Delete comments
    comment_count = str(len(Comment.objects.all()));
    Comment.objects.filter(question=q).delete();
    
    if(q.submitter == request.session.get('uid', 0) or check_admin(request.session.get('nick', 0))):
        q.delete();
        messages.info(request, "Post and " + comment_count + " comments have been successfully deleted");
        return HttpResponseRedirect(reverse('polls:index'));
    else:
        return HttpResponseForbidden();
        
def question_sticky(request, question_id):
    q = get_object_or_404(Question, id=question_id);
    q.sticky = ~q.sticky;
    q.save();
    messages.info(request, "Question stickied successfully.");
    return HttpResponseRedirect(request.GET.get("next", reverse('polls:index')));
        
def post_comment(request, question_id):
    try:
        comment = Comment(
            question = Question.objects.get(id=question_id),
            submitter = request.session['uid'],
            submitter_nick = request.session.get('nick', ""),
            text = sanitizer.clean(request.POST['comment_text'])
            );
        
        comment.full_clean();
        comment.question.last_modified = datetime.datetime.now();
        comment.question.save();
        comment.save();
        
    except ValidationError, e:
        for key in e:
            messages.error(request, "Comment - " + key[1][0]);
    except KeyError, e:
        return HttpResponseBadRequest();
    return HttpResponseRedirect(reverse('polls:detail', kwargs={'question_id': question_id}));
        
def comment_delete(request, comment_id):
    c = get_object_or_404(Comment, id_txt=comment_id);
        
    if(check_admin(request.session.get('nick', 0))):
        c.delete();
        messages.info(request, "Comment successfully deleted");
        return HttpResponseRedirect(request.GET.get("next", reverse('polls:index')));
    else:
        return HttpResponseForbidden();

def create_nick(request): 
    #Verify logged in
    if 'uid' not in request.session:
        return HttpResponseBadRequest();
        
    #Verify POST request
    if 'nick' not in request.POST:
        return HttpResponseBadRequest();
        
    input_post = request.POST['nick'].split(':', 1);
    
    #Determine if it's temporary or not
    if (len(input_post) == 1):
        #Temporary
        if len(input_post[0]) >= 20:
            messages.error(request, "Nickname too long!");
        elif len(Nick.objects.filter(name=input_post[0])) >= 0:
            messages.error(request, "Nickname already taken!");
        else:
            request.session['nick'] = input_post[0];
            messages.warning(request, "Nickname has no password. The nickname will be temporary and available for use by anyone. Use ':' to seperate the name and password");
        return HttpResponseRedirect(request.GET.get("next", reverse('polls:index')));
    else:
        #Not Temporary
        #Get Nick object, veirfy password if exists, create a new one if it doesn't
        try:
            nick = Nick.objects.get(name=input_post[0]);
                
            if nick.verify_password(input_post[1]):
                request.session['nick'] = nick.name;
            else:
                messages.error(request, "Invalid password!");
                    
        except Nick.DoesNotExist:
            #Nick doesn't exist, create a new one
            nick = Nick.create_password(input_post[0], input_post[1]);
            nick.full_clean();
            nick.save();
            request.session['nick'] = nick.name;
            messages.info(request, "Successfuly created new nickname " + nick.name);
            
    messages.info(request, "Successfuly set nickname");
    return HttpResponseRedirect(request.GET.get("next", reverse('polls:index')));
            