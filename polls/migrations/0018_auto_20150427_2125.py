# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('polls', '0017_auto_20150420_2128'),
    ]

    operations = [
        migrations.RenameField(
            model_name='question',
            old_name='sumbitter_nick',
            new_name='submitter_nick',
        ),
    ]
