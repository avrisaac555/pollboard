# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import polls.models


class Migration(migrations.Migration):

    dependencies = [
        ('polls', '0014_auto_20150325_2218'),
    ]

    operations = [
        migrations.AddField(
            model_name='comment',
            name='id_txt',
            field=models.CharField(default=polls.models.comment_id_txt, max_length=6),
            preserve_default=True,
        ),
    ]
