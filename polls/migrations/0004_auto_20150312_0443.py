# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import polls.models


class Migration(migrations.Migration):

    dependencies = [
        ('polls', '0003_question_submitter'),
    ]

    operations = [
        migrations.AlterField(
            model_name='question',
            name='submitter',
            field=models.CharField(default=polls.models.rand_submitter_gen, max_length=40),
            preserve_default=True,
        ),
    ]
