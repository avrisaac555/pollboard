# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('polls', '0019_nick_admin'),
    ]

    operations = [
        migrations.AddField(
            model_name='question',
            name='last_modified',
            field=models.DateTimeField(default=datetime.datetime(2015, 4, 29, 21, 4, 55, 688064, tzinfo=utc), auto_now_add=True),
            preserve_default=False,
        ),
    ]
