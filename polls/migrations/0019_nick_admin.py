# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('polls', '0018_auto_20150427_2125'),
    ]

    operations = [
        migrations.AddField(
            model_name='nick',
            name='admin',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
