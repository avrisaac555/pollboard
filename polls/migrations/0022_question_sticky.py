# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('polls', '0021_auto_20150429_2108'),
    ]

    operations = [
        migrations.AddField(
            model_name='question',
            name='sticky',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
