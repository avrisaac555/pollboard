# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('polls', '0015_comment_id_txt'),
    ]

    operations = [
        migrations.AddField(
            model_name='comment',
            name='submitter_nick',
            field=models.CharField(default=b'', max_length=40, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='question',
            name='sumbitter_nick',
            field=models.CharField(default=b'', max_length=40, blank=True),
            preserve_default=True,
        ),
    ]
