# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import polls.models


class Migration(migrations.Migration):

    dependencies = [
        ('polls', '0002_question_description'),
    ]

    operations = [
        migrations.AddField(
            model_name='question',
            name='submitter',
            field=models.CharField(default=polls.models.rand_submitter_gen, unique=True, max_length=40),
            preserve_default=True,
        ),
    ]
