# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('polls', '0012_auto_20150318_2008'),
    ]

    operations = [
        migrations.AlterField(
            model_name='question',
            name='tagline',
            field=models.CharField(default=b'', max_length=140, blank=True),
            preserve_default=True,
        ),
    ]
