from django.conf.urls import patterns, include, url
from polls import views

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'gettingstartedwithdjango.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^$', views.index, name='index'),
    url(r'^(?P<question_id>\d+)/$', views.detail, name='detail'),
    url(r'^(?P<question_id>\d+)/results/$', views.results, name='results'),
    url(r'^(?P<question_id>\d+)/vote/$', views.vote, name='vote'),
    url(r'^(?P<question_id>\d+)/comment/$', views.post_comment, name='comment'),
    url(r'^(?P<question_id>\d+)/delete/$', views.question_delete, name='question_delete'),
    url(r'^(?P<question_id>\d+)/sticky/$', views.question_sticky, name='question_sticky'),
    url(r'^comment/(?P<comment_id>[\d\w]+)/delete/$', views.comment_delete, name='comment_delete'),
    url(r'^login/$', views.login, name='login'),
    url(r'^post/$', views.post_poll, name='post'),
    url(r'^nick/$', views.create_nick, name='nick'),
)
