import bleach
import markdown
import re
from bs4 import BeautifulSoup

TAGS = ('span', 'p', 'div','h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'strong', 'cite', 'q', 'code', 'a', 'img', 'ul', 'ol',
                'li', 'dl', 'dt', 'dd', 'i', 'tt', 'sub', 'sup', 'hr', 'br');
                
ATTRIBUTES = {
    '*': ['style', 'name'],
    'a': ['href', 'rel', 'title'],
    'img': ['src', 'alt', 'title'],
};

STYLES = ['color', 'font-family', 'font-weight', 'outline', 'text-decoration']

MARKDOWN_EXTENSIONS = ['markdown.extensions.nl2br', 'markdown.extensions.smarty'];

ref_expr = '\+(?P<ref>([a-z0-9]{6}))';

def format_html(html):
    """
    Serches for certain html elements and changes them appropriately
    """
    soup = BeautifulSoup(html);
    return_str = "";
    
    for image_tag in soup.find_all("img"):
        image_tag.parent['class'] = 'img-container';
        
        control_tag = soup.new_tag("a")
        control_tag.string = "Hide";
        control_tag['class'] = "imageHide btn btn-default";
        
        image_tag.insert_before(control_tag);
    
    for tag in soup.body.children:
        return_str += str(tag);
    
    return return_str;

def parse_refs(text):
    """
    Searches for references in commens ( +aabbcc) and generates appropriate HTML
    """
    rg = re.compile(ref_expr);
    return rg.sub('<a class="comment-ref" href="#\g<ref>">+\g<ref></a>', text);

def clean(text):
    parsed_text = markdown.markdown(text, output_format='html5', extensions=MARKDOWN_EXTENSIONS);
    return format_html(parse_refs(bleach.clean(parsed_text, tags=TAGS, attributes=ATTRIBUTES, styles=STYLES)));
    
def clean_all(text):
    return bleach.clean(text, tags=());