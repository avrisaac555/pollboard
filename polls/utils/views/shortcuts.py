from polls.models import Question, Choice, Comment, Nick
from django.contrib import messages

def base_context_builder(request, additional_context=None):
    """
    Builds a dictionary to use for the context parameter on all webpages
    
    Args:
        request: A HttpRequest object
        additional_context: A dictionary of addition items to merge with
    """
    uid = request.session.get('uid', 0);
    nick = None;
    admin_nicknames = [];
    context_dict = {};
    
    #Check for an existing nick object, if it doesn't exist, an exception will be thrown
    try:
        nick = Nick.objects.get(name=request.session['nick']);
    except Nick.DoesNotExist:
        #Temporary, probably
        nick = {'name':request.session['nick']};
    except:
        pass;
    
    for n in Nick.objects.filter(admin=True):
        admin_nicknames.append(n.name);
    
    #Check for additional context
    if additional_context:
        context_dict = additional_context.copy();
        
    context_dict.update({'uid':uid, 'nick':nick, 'admin_nicks':admin_nicknames});
    return context_dict;

def check_admin(nickname):
    """
    Checks the specified nickname for admin credentials
    
    Args:
        nickname: String of the user's nickname
    Returns:
        True, if the user is an admin, False if they are not, or the user doesn't exist.
    """
    try:
        nick = Nick.objects.get(name=nickname);
        
        if (nick.admin):
            return True;
        else:
            return False;
    except Nick.DoesNotExist:
        return False;