from django.contrib import admin
from polls.models import Question, Choice, Comment, Nick

class CommentInline(admin.TabularInline):
    model = Comment
    extra = 0;

class ChoiceInline(admin.TabularInline):
    model = Choice;
    extra = 0;

class QuestionAdmin(admin.ModelAdmin):
    list_display = ('question_text', 'submitter', 'last_modified', 'pub_date', 'was_published_recently');
    list_filter = ['pub_date'];
    fieldsets = [
        (None, {'fields': ('question_text', 'description', 'sticky')}),
        ('Date Information', {'fields': ['pub_date']}),
        ];
    inlines = [ChoiceInline];

class NicknameAdmin(admin.ModelAdmin):
    list_display = ('name', 'password_hash', 'admin');
    list_filter = ['name'];
    fieldsets = [
        (None, {'fields': ('name', 'password_hash', 'admin')}),
    ]
    readonly_fields = ('name',);

# Register your models here.
admin.site.register(Question, QuestionAdmin);
admin.site.register(Nick, NicknameAdmin);
admin.site.register(Comment);