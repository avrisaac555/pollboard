/**
 * Script for adding a dynamic amount of choices to polls on the posting page.
 * 
 * Original Code; Donut Steel
 * */

function setFocus() {
    $('#choice-list .input-group:first').find('input[type="text"]').focus();
}

/**
 * Adds a new input group at the top of the input list
 */
function addButtonListener(e) {
    e.preventDefault();
    
    //Get some DOM elements
    $('#choice-list > h3').after('<div class="input-group p-space">\
                                    <input name="choice" type="text" class="form-control" placeholder="Choice" />\
                                    <span class="input-group-btn">\
                                        <button id="rm-btn" class="btn btn-danger" type="button"><span class="glyphicon glyphicon-minus"></span></button>\
                                    </span>\
                                </div>');
    
    //Add the event listener                            
    $('#rm-btn').on('click', rmButtonListener);
    
    setFocus();
}

/**
 * Removes the first .input-group
 */
function rmButtonListener(e) {
    e.preventDefault(e)
    
    $('#choice-list .input-group:first').remove();
    
    setFocus();
}

$(document).ready(function() {
    
    $('#add-btn').on('click', addButtonListener);
    $('#rm-btn').on('click', rmButtonListener);
    
});