function showCommentPopup(e) {
    var ref_id = $(e.target).attr("href").substring(1);
    var ref_html = $(".comment[id='" + ref_id +"']").html()
    
    $(".ref-popup").html(ref_html);
    $(".ref-popup").css({
        "display": "block",
        "left": (e.pageX + 20) + "px",
        "top": (e.pageY + 10) + "px"
    });
}

function removeCommentPopup(e) {
    $(".ref-popup").css({
        "display": "none"
    });
}

$(document).ready(function() {
    var uid = $("#uid").text();

    /* Show full UID on mouseover */
    $("#uid").text(uid.substring(0, 6));
    $("#uid").parent().hover(
        function() {
            $("#uid").text(uid)
        },
        function() {
            $("#uid").text(uid.substring(0, 6));
        });
    
    /* Show comment popup on mousover */
    $("body").append('<div class="ref-popup"></div>');
        
    $(".comment-ref").on("mousemove", showCommentPopup);
    $(".comment-ref").on("mouseout", removeCommentPopup);
    
    $(".comment .reply-btn").click(function(e) {
        var id = $(this).attr("id");
        $("textarea[name='comment_text']").val(function(i, text) {
            if(text)
                return text + " +" + id;
            else
                return text + "+" + id;
        });
        $('html, body').animate({scrollTop: $("#comment-box").offset().top}, 10);
    });
    
    /* Image controls */
    $(".img-container img").toggle();
    $(".img-container .imageHide").text('Reveal Image');
    
    $(".img-container").hover(
        function(e) {
            $(this).find(".imageHide").show();
        }, 
        function(e) {
            if($(this).find("img").is(":visible"))
                $(this).find(".imageHide").hide();
        });
    
    $(".img-container .imageHide").click(function(e) {
        $(this).parent().children("img").toggle();
        if($(this).text() == "Hide Image") {
            $(this).css("position", "static");
            $(this).text("Reveal Image");
        } else {
            $(this).css("position", "absolute");
            $(this).text("Hide Image");
        }
    });
    
});
